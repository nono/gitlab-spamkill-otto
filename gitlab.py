#encoding: utf-8
"""
Ensemble d'utilitaires pour gitlab, via son API.
"""

import requests

GITLAB_URL = "https://gitlab.example.net/api/v4/"
PRIVATE_TOKEN = ""

# Création d'une nouvelle Session
session = requests.Session()
session.headers.update({
    "PRIVATE-TOKEN": PRIVATE_TOKEN,
})
parameters = {
    "per_page": 100,
    "page": 1,
}

def list_items(object_name, page=1):
    parameters["page"] = page
    response = session.get(GITLAB_URL + object_name, params=parameters)
    if response.encoding is None:
        response.encoding = "ISO-8859-1"
    if response.status_code==200:
        return response.json()
    return []

def list_objects(object_name):
    objects = []
    page = 1
    temp_objects = list_items(object_name, page)
    while temp_objects!=[]:
        objects.extend(temp_objects)
        page += 1
        temp_objects = list_items(object_name, page)
    return objects

def list_groups():
    return list_objects("groups")

def list_projects():
    return list_objects("projects")

def list_users():
    return list_objects("users")

def user_detail(user_id):
    response = session.get(GITLAB_URL + "users/%d" % user_id)
    if response.encoding is None:
        response.encoding = "ISO-8859-1"
    if response.status_code==200:
        return response.json()
    return []


def delete_user(user_id):
    data = {
        "hard_delete": True,
    }
    response = session.delete(GITLAB_URL + "users/%d" % user_id, data=data)
