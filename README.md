# Otto : Gitlab Spam Kill

> Développé par Mindiell pour La Quadrature du Net

## Usage

Avant toute chose, remplissez les variables `GITLAB_URL` et `PRIVATE_TOKEN` dans le fichier `gitlab.py`. [Voir la documentation pour obtenir un token.](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html)

Tout d'abord, nous allons lancer le script permettant de lister nouveaux utilisateurices des 30 derniers jours avec ;

`python otto.py last users 30`

Une fois le spam identifié avec nos beaux yeaux, nous pouvons lancer la suppression des utilisateurices avec la commande suivante ;

`python otto.py delete users 456-457`

Pour supprimer les utilisateurs dont l'id est situé entre 456 et 457

Il existe une whitelist, dans le fichier `whitelist.json`, qui permet d'éviter la suppression des utiliateurices, malgré leur présence dans l'interval précédent.

> A utiliser avec l'amie "Parcimonie" donc ;o)
