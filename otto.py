#encoding: utf-8
"""
Otto liste les groupes, les projets, et les utilisateurs d'un site gitlab
afin de permettre à des humains de signaler du spam simplement.

options :
- list groups : Liste les groupes inconnus
- add group name : Ajoute le groupe "name" aux groupes connus
"""

from datetime import datetime, timedelta
import json
import sys
import time

from gitlab import list_groups, list_projects, list_users, delete_user, user_detail

if __name__=="__main__":
    if len(sys.argv)<3:
        print("usage")
        exit()

    elif len(sys.argv)==3:
        if sys.argv[1]=="search":
            # Chargement des listes blanches
            with open("whitelists.json") as f:
                whitelists = json.loads(f.read())

            if sys.argv[2]=="groups":
                # On parcourt l'ensemble des groupes afin de signaler les inconnus
                for group in list_groups():
                    if group["name"] not in whitelists["groups"]:
                        print(group["name"])

            elif sys.argv[2]=="projects":
                # On parcourt l'ensemble des projets afin de signaler les inconnus
                for project in list_projects():
                    if project["name"] not in whitelists["projects"] and project["namespace"]["name"] not in whitelists["namespaces"]:
                        print("%s (%s)" % (project["name"], project["namespace"]["name"]))

            elif sys.argv[2]=="users":
                # On parcourt l'ensemble des utilisateurs afin de signaler les inconnus
                for user in list_users():
                    if user["name"] not in whitelists["users"] and user["name"] not in whitelists["namespaces"]:
                        found = False
                        for domain in whitelists["domains"]:
                            if user["email"].endswith(domain):
                                found = True
                                break
                        if not found:
                            print("%5d: %s - %s" % (user["id"], user["name"], user["email"]))

            else:
                print("unknown option %s" % sys.argv[2])

        elif sys.argv[1]=="list":
            # Chargement des listes blanches
            with open("whitelists.json") as f:
                whitelists = json.loads(f.read())

            if sys.argv[2] in ("groups", "projects", "users", "namespaces", "domains"):
                for item in whitelists[sys.argv[2]]:
                    print(item)

            else:
                print("unknown option %s" % sys.argv[2])

        elif sys.argv[1]=="clear":
            # Chargement des listes blanches
            with open("whitelists.json") as f:
                whitelists = json.loads(f.read())

            if sys.argv[2] in ("groups", "projects", "users", "namespaces", "domains"):
                whitelists[sys.argv[2]].clear()
                with open("whitelists.json", "w") as f:
                    json.dump(whitelists, f, indent=4)

            else:
                print("unknown option %s" % sys.argv[2])


    elif len(sys.argv)==4:
        if sys.argv[1]=="add":
            # Chargement des listes blanches
            with open("whitelists.json") as f:
                whitelists = json.loads(f.read())

            if sys.argv[2] in ("group", "project", "user", "namespace", "domain"):
                whitelists["%ss" % sys.argv[2]].append(sys.argv[3])
                with open("whitelists.json", "w") as f:
                    json.dump(whitelists, f, indent=4)

            else:
                print("unknown option %s" % sys.argv[2])

        elif sys.argv[1]=="remove":
            # Chargement des listes blanches
            with open("whitelists.json") as f:
                whitelists = json.loads(f.read())

            if sys.argv[2] in ("group", "project", "user", "namespace", "domain"):
                whitelists["%ss" % sys.argv[2]].remove(sys.argv[3])
                with open("whitelists.json", "w") as f:
                    json.dump(whitelists, f, indent=4)

            else:
                print("unknown option %s" % sys.argv[2])

        elif sys.argv[1]=="last":
            # Chargement des listes blanches
            with open("whitelists.json") as f:
                whitelists = json.loads(f.read())

            if sys.argv[2]=="groups":
                # On parcourt l'ensemble des groupes afin de signaler les inconnus
                for group in list_groups():
                    if group["name"] not in whitelists["groups"]:
                        print(group["name"])

            elif sys.argv[2]=="projects":
                # On parcourt l'ensemble des projets afin de signaler les inconnus
                for project in list_projects():
                    if project["name"] not in whitelists["projects"] and project["namespace"]["name"] not in whitelists["namespaces"]:
                        print("%s (%s)" % (project["name"], project["namespace"]["name"]))

            elif sys.argv[2]=="users":
                print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                # On parcourt l'ensemble des utilisateurs afin de signaler les inconnus
                for user in list_users():
                    if datetime.strptime(user["created_at"][:19], "%Y-%m-%dT%H:%M:%S")>(datetime.now()-timedelta(days=int(sys.argv[3]))):
                        if user["name"] not in whitelists["users"] and user["name"] not in whitelists["namespaces"]:
                            found = False
                            for domain in whitelists["domains"]:
                                if user["email"].endswith(domain):
                                    found = True
                                    break
                            if not found:
                                print("%5d: %s - %s" % (user["id"], user["name"], user["email"]))
                                #print(json.dumps(user, indent=4))
                                #print(json.dumps(user_detail(user["id"]), indent=4))

            else:
                print("unknown option %s" % sys.argv[2])

        elif sys.argv[1]=="delete":
            if sys.argv[2]=="users":
                # On supprime l'ensemble des utilisateurs dont les ids sont compris entre début et fin
                debut = int(sys.argv[3].split("-")[0])
                fin = int(sys.argv[3].split("-")[1])
                for user_id in range(debut, fin+1):
                    delete_user(user_id)

            else:
                print("unknown option %s" % sys.argv[2])

        else:
            print("unknown command %s" % sys.argv[1])
